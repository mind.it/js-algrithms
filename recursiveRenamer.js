
const fs=require('fs');
const i18n = require('transliteration');


function getFiles (dir, files_){
  files_ = files_ || [];
  let files = fs.readdirSync(dir);
  for (let i in files){
    let name = dir + '/' + files[i];
    if (fs.statSync(name).isDirectory()){
      getFiles(name, files_);
    } else {
      files_.push(name);
    }
  }
  return files_;
}

const filesArray = getFiles('./files');

filesArray.reverse().forEach((file) => {
  const fileArray = file.split('/');
  const filename = fileArray[4];
  const newFilename = i18n.slugify(filename);
  const newFileArray = JSON.parse(JSON.stringify(fileArray));
  
  newFileArray[4] = newFilename;
  
  const oldPath = fileArray.join('/');
  const newPath = newFileArray.join('/');
  
  fs.rename(oldPath, newPath, function(err) {
    if ( err ) console.log('ERROR: ' + err);
  });
})


