const data = [1, 99, 64, 32, 45, 14, 51, 21, 100, 63, 62, 55, 2, 4, 9];

function sortQuick(arrayNumber){
    function swap(items, firstIndex, secondIndex) {
        const temp = items[firstIndex];
        items[firstIndex] = items[secondIndex];
        items[secondIndex] = temp;
    }
    
    function partition(items, left, right) {
        let medi = items[Math.floor((right + left) / 2)], i = left, j = right;
        while (i <= j) {
            while (items[i] < medi) {
                i++;
            }
            while (items[j] > medi) {
                j--;
            }
            if (i <= j) {
                swap(items, i, j);
                i++;
                j--;
            }
        } return i;
    }
    
    function sort(items, left, right) {
        let index;
        if (items.length > 1) {
            left = typeof left != "number" ? 0 : left;
            right = typeof right != "number" ? items.length - 1 : right;
            index = partition(items, left, right);
            if (left < index - 1) {
                sort(items, left, index - 1);
            } if (index < right) {
                sort(items, index, right);
            }
        } return items;
    }
    return sort(arrayNumber);
}